package ru.t1.dkozoriz.tm.util;


import java.text.DecimalFormat;

public interface FormatUtil {

    static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");

    static final long KILOBYTE = 1024;

    static final double MEGABYTE = KILOBYTE * 1024;

    static final double GIGABYTE = MEGABYTE * 1024;

    static final double TERABYTE = GIGABYTE * 1024;

    String NAME_BYTES = "B";

    String NAME_KILOBYTE = "KB";

    String NAME_MEGABYTE = "MB";

    String NAME_GIGABYTE = "GB";

    String NAME_TERABYTE = "TB";

    String NAME_BYTES_LONG = "Bytes";

    String SEPARATOR = " ";

    static String render(final double bytes) {
        return DECIMAL_FORMAT.format(bytes);
    }

    static String render(final long bytes, final double size) {
        return render(bytes / size);
    }

    static String render(final long bytes, final double size, final String name) {
        return render(bytes, size) + SEPARATOR + name;
    }

    static String render(final long bytes, final String name) {
        return render(bytes) + SEPARATOR + name;
    }


    public static String formatBytes(final long bytes) {
        if ((bytes >= 0) && (bytes < KILOBYTE)) return render(bytes, NAME_BYTES);
        if ((bytes >= KILOBYTE) && (bytes < MEGABYTE)) return render(bytes, KILOBYTE, NAME_KILOBYTE);
        if ((bytes >= MEGABYTE) && (bytes < GIGABYTE)) return render(bytes, MEGABYTE, NAME_MEGABYTE);
        if ((bytes >= GIGABYTE) && (bytes < TERABYTE)) return render(bytes, GIGABYTE, NAME_GIGABYTE);
        if (bytes >= TERABYTE) return render(bytes, TERABYTE, NAME_TERABYTE);
        return render(bytes, NAME_BYTES_LONG );
    }

}