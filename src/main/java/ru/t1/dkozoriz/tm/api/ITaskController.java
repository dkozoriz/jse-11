package ru.t1.dkozoriz.tm.api;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}