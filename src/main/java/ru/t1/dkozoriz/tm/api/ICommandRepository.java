package ru.t1.dkozoriz.tm.api;

import ru.t1.dkozoriz.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}