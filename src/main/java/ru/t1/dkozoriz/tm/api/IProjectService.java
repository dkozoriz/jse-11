package ru.t1.dkozoriz.tm.api;

import ru.t1.dkozoriz.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}