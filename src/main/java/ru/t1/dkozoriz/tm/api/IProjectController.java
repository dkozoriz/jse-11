package ru.t1.dkozoriz.tm.api;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}